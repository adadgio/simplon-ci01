import * as fastify from "fastify"
import { Something } from "./service/something"

/**
 * Hello world.
 */
const app = () => {

    const server = fastify()
    
    server.route({
        method: 'GET',
        url: '/',
        schema: {
            //querystring: {
            //    name: { type: 'string' },
            //    excitement: { type: 'integer' }
            //},
            response: {
                200: {
                    type: 'object',
                    properties: {
                        hello: { type: 'string' }
                    }
                }
            }
        },
        handler: (request, reply) => {
            const value = Something.getNumber()
            reply.send({ number: value })
        }
    })

    server.listen(3000, "0.0.0.0")
}

export default app
