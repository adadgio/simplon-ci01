import axios from "axios"
const config = require("../config/config.json")

class SomethingService
{
    $number: number
    constructor()
    {
        this.$number = 42
    }

    getNumber()
    {
        return this.$number
    }
}

export const Something = new SomethingService()
