import * as chai from 'chai'
import * as assert from 'assert'
const expect = chai.expect

//import app from "../src/app"
import { Something } from "../src/service/something"

describe("Something service test", () => {

    it("Something::getNumber() should return a correct value", () => {
        const number = Something.getNumber()
        expect(number).to.equal(42)
    })

})
